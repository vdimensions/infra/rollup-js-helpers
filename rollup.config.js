import peerDepsExternal from "rollup-plugin-peer-deps-external";
import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import copy from "rollup-plugin-copy";

import { embedLicense, prepareChunks } from "./src/index";


export default {
  input: prepareChunks({ workDir: `${__dirname}/src/`, paths : ["index.js" ] }),
  output: [
    {
      dir: "dist",
      format: "cjs",
      sourcemap: true
    }
  ],
  //manualChunks: manualChunks("src/"),
  plugins: [
    peerDepsExternal(),
    resolve(),
    commonjs(),
    copy({
      targets: [
        {
          src: "package.json",
          dest: "dist"
        }
      ]
    }),
    embedLicense({ workDir: __dirname }),
  ]
};
