const path = require('path');
const glob = require('glob');

export function prepareChunks({workDir = __dirname,  paths = []}) {
    return paths
        .reduce(
            (arr, p) => {
                return [...arr, ...glob.sync(path.join(workDir, p)).map(p => {
                    var targetPath = path.parse(path.relative(workDir, p));
                    var key = path.join(targetPath.dir, targetPath.name);
                    return [key, p];
                }) ];
            }, 
            [])
        .reduce(
            (obj, [key, item]) => {
                return { 
                    ...obj, 
                    [key]: item
                }
            },
            {});
};