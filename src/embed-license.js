const path = require('path');

export function embedLicense({workDir = __dirname}) {
  const license = require("rollup-plugin-license");
  return license({
    sourcemap: true,
    cwd: workDir,//'.', // Default is process.cwd()

    banner: {
      commentStyle: 'regular', // The default

      content: {
        file: path.join(workDir, 'LICENSE'),
        encoding: 'utf-8', // Default is utf-8
      },

      // // Optional, may be an object or a function returning an object.
      // data() {
      //   return {
      //   };
      // },
    },

    // thirdParty: {
    //   includePrivate: false, // Default is false.
    //   output: {
    //     file: path.join(workDir, 'dist', 'dependencies.txt'),
    //     encoding: 'utf-8', // Default is utf-8.
    //   },
    // },
  });
};